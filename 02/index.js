// Реализовать программу на Javascript, которая будет находить все числа кратные 5 (делятся на 5 без остатка) в заданном диапазоне. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
// Технические требования:
// Считать с помощью модального окна браузера число, которое введет пользователь.
// Вывести в консоли все числа кратные 5, от 0 до введенного пользователем числа. Если таких чисел нету - вывести в консоль фразу `Sorry, no numbers'
// Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.


// init variables
let counter = 0;
let userNumber = parseInt(prompt("Enter number", 55));
// Check if userNumber has typeof number and it is Integer
while (isNaN(userNumber) && !Number.isInteger(userNumber)) {
  userNumber = parseInt(prompt("Enter number", 55));
}
for (let i = 0; i <= userNumber; i++) {
  if (userNumber < 5) {
    console.log('Sorry no numbers');
    // exit from the loop
    break;
  } else if (i % 5 === 0) {
    counter++;
    console.log(i);
  }
}
console.log(`multiples counts amount: ${counter}`);

// Необязательное задание продвинутой сложности:
// Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять вывод окна на экран до тех пор, пока не будет введено целое число.
// Считать два числа, m и n. Вывести в консоль все простые числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше, вывести сообщение об ошибке, и спросить оба числа заново.


// ADVANCED
// let m = parseInt(prompt("Enter first", 1));
// let n = parseInt(prompt("Enter second", 55));

// while (isNaN(m) && !Number.isInteger(m) && isNaN(n) && !Number.isInteger(n) || m > n) {
//   alert("Please type a valid numbers")
//   m = parseInt(prompt("Enter first", 1));
//   n = parseInt(prompt("Enter second", 55));
// }


// for (let i = m; i <= n; i++) {
//   let switcher = 1;
//   if (i > 2 && i % 2 !== 0) {
//     for (let k = 2; k * k <= i; k = k + 2) {
//       if (i % k === 0) {
//         switcher = 0;
//         break;
//       }
//     }
//   } else if (i !== 2) {
//     switcher = 0;
//   }
// Check to show digit
//   if (switcher === 1) {
//     console.log(i);
//   }
// }