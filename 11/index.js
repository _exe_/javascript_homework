const buttons = document.querySelectorAll('.btn');
const buttonsValues = Array.from(buttons).map(element => element.textContent.toLowerCase());
let previousBtn = null;

const drawBtn = function (currentbtn) {
  const buttonsArray = Array.from(buttons);
  if (previousBtn === currentbtn) {
    for (let j = 0; j < buttonsArray.length; j++) {
      const element = buttonsArray[j];
      element.style.backgroundColor = '#33333a';
    }
  } else if (previousBtn === null) {
    for (let i = 0; i < buttonsArray.length; i++) {
      const element = buttonsArray[i];
      const tagText = element.textContent.toLowerCase();
      if (tagText === currentbtn) {
        element.style.backgroundColor = 'blue';
      }
    }
    previousBtn = currentbtn;
  } else if (previousBtn !== currentbtn) {
    for (let j = 0; j < buttonsArray.length; j++) {
      const element = buttonsArray[j];
      element.style.backgroundColor = '#33333a';
      const tagText = element.textContent.toLowerCase();

      if (tagText === currentbtn) {
        element.style.backgroundColor = 'blue';
      }
    }
    previousBtn = currentbtn;
  }
};

const showKeyName = function (event) {
  const lowerEvent = event.key.toLowerCase()
  const findBtn = buttonsValues.find(element => element === lowerEvent);
  if (findBtn !== undefined) {
    drawBtn(findBtn);
  }
};

document.addEventListener('keypress', showKeyName);