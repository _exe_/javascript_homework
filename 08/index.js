const input = document.createElement('input');
input.setAttribute('placeholder', 'price');

const addInput = function () {
  document.body.prepend(input);
};

const addBorder = function (evt) {
  evt.target.style.cssText = 'border: 1px solid green; outline: 1px solid green;';
};

const showError = function (element) {
  element.style.cssText = 'background-color: red;';
  const error = document.createElement('span');
  error.textContent = 'Please enter correct price';
  element.after(error);
  setTimeout(() => {
    error.remove();
    element.value = '';
    element.style.cssText = 'background-color: transparent';
  }, 1500);
};

const countPrice = function (element) {
  element.style.cssText = 'background-color: green';
  const priceSpan = document.createElement('span');
  priceSpan.innerHTML = `Текущая цена: ${element.value} <button>X</button>`;
  element.before(priceSpan);
  removeSpan(priceSpan);
};

const removeSpan = function (element) {
  element.addEventListener('click', function (evt) {
    if (evt.target.tagName === 'BUTTON') {
      evt.target.parentElement.remove();
      input.value = '';
      input.style.cssText = 'background-color: transparent';
    }
  });
};

const showPrice = function (evt) {
  evt.target.style.cssText = 'border: 1px solid transparent; outline: 1px solid transparent;';
  if (evt.target.value <= 0) {
    showError(evt.target);
  } else {
    countPrice(evt.target);
  }
};

window.document.addEventListener('DOMContentLoaded', addInput);
input.addEventListener('focus', addBorder);
input.addEventListener('blur', showPrice);