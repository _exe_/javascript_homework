const countryList = document.createElement('ul');
countryList.classList.add("country-list");
document.body.append(countryList);

const countries = ['Ukraine', ['Kiev', 'Kharkiv', 'Odessa', 'Lviv'], '日本', ['東京', ['さいたま', '上野'], '大阪', '北海道' ], ];

const createList = function (parent, array) {

  const newArray = array.map((item) => {
    const li = document.createElement('li');
    if (item instanceof Array) {
      let ul = document.createElement('ul');
      li.append(ul);
      createList(ul, item);
    } else {
      li.textContent = item;
    }
    return li;
  });
  newArray.forEach(element => parent.append(element));
};

createList(document.querySelector(".country-list"), countries);