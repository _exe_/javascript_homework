const createNewUser = function (firstName = prompt("First name", "John"), lastName = prompt("Last name", "Doe")) {

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function () {
      return ((this.firstName[0] + this.lastName).toLowerCase());
    }
  };
  return newUser;

};
const result = createNewUser("Andrew", "Jackson");
console.log("User login is: " + result.getLogin());