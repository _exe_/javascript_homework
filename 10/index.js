const icon = document.querySelectorAll('i');
const form = document.querySelector('.password-form');
const dataInput = document.querySelectorAll('input');

const submitForm = function (evt) {
  evt.preventDefault();
  const passwords = Array.from(dataInput).map(element => element.value);
  const isValid = passwords.every(v => v === passwords[0]);

  if (isValid) {
    alert("You are welcome");
  } else {
    const error = document.createElement('p');
    error.style.color = 'red';
    error.textContent = 'Нужно ввести одинаковые значения';
    dataInput[dataInput.length - 1].after(error);
  }
};

const showPassword = function (evt) {
  const TagI = evt.target;
  if (TagI.classList.contains('fa-eye')) {
    TagI.classList.remove('fa-eye');
    TagI.classList.add('fa-eye-slash');
    TagI.previousElementSibling.setAttribute('type', 'text');
  } else {
    TagI.classList.remove('fa-eye-slash');
    TagI.classList.add('fa-eye');
    TagI.previousElementSibling.setAttribute('type', 'password');
  }
};

icon.forEach(element => {
  element.addEventListener('click', showPassword);
});

form.addEventListener('submit', submitForm);