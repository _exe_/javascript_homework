const createNewUser = function (firstName = prompt("First name", "John"), lastName = prompt("Last name", "Doe")) {
  const birthDate = prompt("Type your birthday in dd.mm.yyyy", "01.07.2000").split(".").reverse();

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function () {
      return ((this.firstName[0] + this.lastName).toLowerCase());
    },
    getAge: function () {
      const TODAY = new Date();
      const birthDateFormatted = new Date(birthDate);
      console.log(birthDateFormatted)
      console.log(TODAY)

      // if birthday has passed
      if (TODAY.getMonth() - birthDateFormatted.getMonth() >= 0 && TODAY.getDate() - birthDateFormatted.getDate() >= 0) {
        return TODAY.getFullYear() - birthDateFormatted.getFullYear();
      } else {
        return ((TODAY.getFullYear() - birthDateFormatted.getFullYear()) - 1);
      }
    },
    getPassword: function () {
      return (firstName[0].toUpperCase() + lastName.toLowerCase() + birthDate[0]);
    }
  };
  return newUser;
};

const result = createNewUser("Andrew", "Jackson");

console.log("User login is: " + result.getLogin());
console.log("User age is: " + result.getAge());
console.log("User password is: " + result.getPassword());